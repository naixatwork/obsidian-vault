`controller` is a `class` [[Sara's backend journey/class (OOP)|class (OOP)]] marked with `@Controller` decorator [[Decorator (Design Pattern)]] it is responsible for defining **API** ([[API]]) over **http** protocol.
we can configure:
- http route
- http methods [[http methods (http)]]
- status code [[http status codes (http)]]

```ts
import { Controller, Get } from '@nestjs/common';  
  
@Controller()  
export class AppController {  
  @Get()  
  getHello(): string {  
    return 'Hello sara';  
  }  
}
```

here the **http route** will be `/` it can be specified in `@Controller()`

```ts
@Controller('test')
```

`getHello()` method has `@Get()` which will be it's **http method**

```ts
@Get()  
getHello(): string {  
	return 'Hello sara';  
}  
```

since this will return a `string` the **status code** will be `OK (200)`
- what's not important is `getHello` function name because we are communicating over **http protocol** it doesn't even matter which language both backend and frontend are developing so anything related to function name doesn't matter.

#### Resource handling using Controllers
define your resource as a class [[Sara's backend journey/class (OOP)|class (OOP)]]

```ts
class Cafe {  
  name: string;  
  rating: number;  
  
  constructor(newName: string, newRating: number) {  
    this.name = newName;  
    this.rating = newRating;  
  }  
}  
  
export default Cafe;
```


