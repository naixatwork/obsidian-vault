`main.ts` is file where nest application gets started this is where most of our **global configurations** will be written.

```ts
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
}
bootstrap();
```

`bootstrap` is an `async function` [[async & await (JS)]].
