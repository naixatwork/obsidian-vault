- https://melt-ui.com/ -> https://www.bits-ui.com/docs/introduction
	- low level
	- custom theme
- https://shadcn-svelte.com/
	- no npm package
	- low level
	- complete customization
- https://www.skeleton.dev/
	- high level
	- not the most complete
	- figma design
	- custom theme
- https://flowbite-svelte.com/docs/pages/introduction
	- high level
	- beta version has flagged date picker as experimental
- material ui
	- not integratable
- bootstrap
	- meh.
- framework agnostics, daisyUI etc etc
	- meh.

more stuff
https://www.youtube.com/watch?v=qyG-xWjNZKU